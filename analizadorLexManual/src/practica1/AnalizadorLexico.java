package practica1;

import java.io.FileInputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.IOException;

public class AnalizadorLexico {

   private Reader input;
   private StringBuffer lex;
   private int sigCar;
   private int filaInicio;
   char prueba;
   private int columnaInicio;
   private int filaActual;
   private int columnaActual;
   private static String NL = System.getProperty("line.separator");
   
   private static enum Estado {
    inicio, recMul, recDiv, recParentesisApertura, recParentesisCierre, recPuntoYcoma, recIgual,
    recMas, recMenos, recID, recEntero, recParteDecimal, recE, recSignoExp, recExp, recEOF, recComm,
    recDistinto, recIgualIgual,recMayorIgual,recMayor, recMenorIgual, recMenor, recPunto, recD, recSep1, recSeparador
    
   }

   private Estado estado;

   public AnalizadorLexico(Reader input) throws IOException {
    this.input = input;
    lex = new StringBuffer();
    sigCar = input.read();
    prueba=(char) sigCar;
    filaActual=1;
    columnaActual=1;
   }
   
   public UnidadLexica sigToken() throws IOException {
     estado = Estado.inicio;
     filaInicio = filaActual;
     columnaInicio = columnaActual;
     lex.delete(0,lex.length());
     while(true) {
         switch(estado) {
           case inicio: 
              if(hayLetra())  transita(Estado.recID);
              else if (hayDigito()) transita(Estado.recEntero);             
              else if (haySuma()) transita(Estado.recMas);
              else if (hayResta()) transita(Estado.recMenos);
              else if (hayMul()) transita(Estado.recMul);
              else if (hayDiv()) transita(Estado.recDiv);
              else if (hayPAp()) transita(Estado.recParentesisApertura);
              else if (hayPCierre()) transita(Estado.recParentesisCierre);
              else if (hayIgual()) transita(Estado.recIgual);
              else if (hayPuntoYcoma()) transita(Estado.recPuntoYcoma);
              else if (hayAlmohadilla()) transitaIgnorando(Estado.recComm);
              else if (haySep()) transitaIgnorando(Estado.inicio);
              else if (hayEOF()) transita(Estado.recEOF);              
              else if (hayD()) transita(Estado.recD);
              else if (hayMayor()) transita(Estado.recMayor);
              else if (hayMenor()) transita(Estado.recMenor); 
              else if (hayUpperAnd()) transita(Estado.recSep1); 
              else error();
              break;
           case recID: 
              if (hayLetra() || hayDigito() || hayBarra()) transita(Estado.recID);
              else return unidadId();               
              break;
           case recEntero:
               if (hayDigito()) transita(Estado.recEntero);
               else if (hayPunto()) transita(Estado.recPunto);
               else if (hayE()) transita(Estado.recE);
               else return unidadNumero();
               break;
           case recPunto:
               if (hayDigito()) transita(Estado.recParteDecimal);
               else error();
               break;
           case recParteDecimal:
        	   if (hayDigito()) transita(Estado.recParteDecimal);               
               else if (hayE()) transita(Estado.recE);
               else return unidadNumero();
               break;
           case recE:
        	   if (hayDigito()) transita(Estado.recExp);               
               else if (haySigno()) transita(Estado.recSignoExp);
               else error();
               break;
           case recSignoExp:
        	   if (hayDigito()) transita(Estado.recExp);  
               else error();
               break;
           case recExp:
        	   if (hayDigito()) transita(Estado.recExp);               
               else return unidadNumero();            
               break;               
           case recMas:
               if (hayDigito()) transita(Estado.recEntero);              
               else return unidadMas();
               break;
           case recMenos: 
               if (hayDigito()) transita(Estado.recEntero);              
               else return unidadMenos();
               break;
           case recMul: return unidadPor();
           case recDiv: return unidadDiv();              
           case recParentesisApertura: return unidadPAp();
           case recParentesisCierre: return unidadPCierre();
           case recIgual: 
        	   if (hayIgual()) transita(Estado.recIgualIgual); 
        	   else return unidadIgual();
        	   break;        	   
           case recPuntoYcoma: return unidadPuntoYComa();
           
           case recComm: 
               if (hayNL()) transitaIgnorando(Estado.inicio);
               else if (hayEOF()) transita(Estado.recEOF);
               else transitaIgnorando(Estado.recComm);
               break;
               
           case recEOF: return unidadEof();
           case recMenor: 
        	   if (hayIgual())  transita(Estado.recMenorIgual);
        	   else return unidadMenor();   
        	   break;
           case recMayor:
        	   if (hayIgual())  transita(Estado.recMayorIgual);
        	   else return unidadMayor();  
        	   break;
           case recMenorIgual:
        	   return unidadMenorIgual();         	   
           case recMayorIgual:        	 
        	    return unidadMayorIgual();          	 
           case recIgualIgual:        	 
        	    return unidadIgualIgual();         	   
           case recD:
        	   if (hayIgual())  transita(Estado.recDistinto);
        	   else error();
        	   break;
           case recDistinto:  
        	   return unidadDistinto();    
           case recSep1:
        	   if (hayUpperAnd())  transita(Estado.recSeparador);
        	   else error();
        	   break;
           case recSeparador:
        	   return unidadSeparador();
         }
     }    
   }
  





private void transita(Estado sig) throws IOException {
     lex.append((char)sigCar);
     sigCar();         
     estado = sig;
   }
   private void transitaIgnorando(Estado sig) throws IOException {
     sigCar();         
     filaInicio = filaActual;
     columnaInicio = columnaActual;
     estado = sig;
   }
   private void sigCar() throws IOException {
     sigCar = input.read();
     if (sigCar == NL.charAt(0)) saltaFinDeLinea();
     if (sigCar == '\n') {
        filaActual++;
        columnaActual=0;
     }
     else {
       columnaActual++;  
     }
   }
   private void saltaFinDeLinea() throws IOException {
      for (int i=1; i < NL.length(); i++) {
          sigCar = input.read();
          if (sigCar != NL.charAt(i)) error();
      }
      sigCar = '\n';
   }
   
   private boolean hayLetra() {return sigCar >= 'a' && sigCar <= 'z' ||
                                      sigCar >= 'A' && sigCar <= 'z';}
   private boolean hayDigitoPos() {return sigCar >= '1' && sigCar <= '9';}
   private boolean hayBarra() {return sigCar =='_';};
   private boolean hayE() {return sigCar =='e' || sigCar =='E';}
   private boolean hayCero() {return sigCar == '0';}
   private boolean hayDigito() {return hayDigitoPos() || hayCero();}
   private boolean haySuma() {return sigCar == '+';}
   private boolean hayResta() {return sigCar == '-';}
   private boolean hayMul() {return sigCar == '*';}
   private boolean hayDiv() {return sigCar == '/';}
   private boolean hayPAp() {return sigCar == '(';}
   private boolean hayPCierre() {return sigCar == ')';}
   private boolean hayIgual() {return sigCar == '=';}
   private boolean hayPuntoYcoma() {return sigCar == ';';}
   private boolean hayPunto() {return sigCar == '.';}
   private boolean hayAlmohadilla() {return sigCar == '#';}
   private boolean haySep() {return sigCar == ' ' || sigCar == '\t' || sigCar=='\n';}
   private boolean hayNL() {return sigCar == '\r' || sigCar == '\b' || sigCar == '\n';}
   private boolean hayEOF() {return sigCar == -1;}
   private boolean hayD() {return sigCar == '!';}
   private boolean hayMayor() {	return sigCar == '>';}
   private boolean hayMenor() {	return sigCar == '<';}
   private boolean haySigno() {	return sigCar == '+' || sigCar == '-';}
   private boolean hayUpperAnd() {	return sigCar == '&';}
   private UnidadLexica unidadId() {
     switch(lex.toString()) {
         case "bool":  
            return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.BOOL);
         case "num":    
            return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.NUM);
         case "and":    
             return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.AND);
         case "or":    
             return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.OR);
         case "not":    
             return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.NOT);
         case "true":    
             return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.CIERTO);
         case "false":    
             return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.FALSO); 
         default:    
            return new UnidadLexicaMultivaluada(filaInicio,columnaInicio,ClaseLexica.IDEN,lex.toString());     
      }
   }  
   private UnidadLexica unidadNumero() {
     return new UnidadLexicaMultivaluada(filaInicio,columnaInicio,ClaseLexica.NUMERO,lex.toString());     
   }   
    
   private UnidadLexica unidadMas() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MAS);     
   }    
   private UnidadLexica unidadMenos() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MENOS);     
   }    
   private UnidadLexica unidadPor() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.POR);     
   }    
   private UnidadLexica unidadDiv() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.DIV);     
   }    
   private UnidadLexica unidadPAp() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.PAP);     
   }    
   private UnidadLexica unidadPCierre() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.PCIERRE);     
   }    
   private UnidadLexica unidadIgual() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.IGUAL);     
   }    
   private UnidadLexica unidadPuntoYComa() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.PUNTOYCOMA);     
   }    
   private UnidadLexica unidadEof() {
     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.EOF);     
   }  
   private UnidadLexica unidadSeparador() {		
	   return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.SEPSEC); 
	}
   
   
   private UnidadLexica unidadMayor() {
	     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MAYOR);     
	   }    
	   private UnidadLexica unidadMenor() {
	     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MENOR);     
	   }    
	   private UnidadLexica unidadMayorIgual() {
	     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MAYORIGUAL);     
	   }    
	   private UnidadLexica unidadDistinto() {
	     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.DISTINTO);     
	   }   	
	   private UnidadLexica unidadMenorIgual(){
		     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.MENORIGUAL);}
	   
	   private UnidadLexica unidadIgualIgual() {
		     return new UnidadLexicaUnivaluada(filaInicio,columnaInicio,ClaseLexica.IGUALIGUAL);}   
  
   private void error() {
     System.err.println("("+filaActual+','+columnaActual+"):Caracter inexperado");  
     System.exit(1);
   }

   public static void main(String arg[]) throws IOException {
     Reader input = new InputStreamReader(new FileInputStream(arg[0]));
     AnalizadorLexico al = new AnalizadorLexico(input);
     UnidadLexica unidad;
     int contador=0;
     do {
       unidad = al.sigToken();
       System.out.println(unidad);
       contador++;
     }
     while (unidad.clase() != ClaseLexica.EOF);
     System.out.println("Total token reconocido = "+contador);
    } 
}